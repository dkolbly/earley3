# Earley Parser

This repository contains an Earley parser implementation for Go, based
on my dissertation work from waaaaay back but ported to be useful in
Go for building DSLs.

## Metagrammar

A simple notation for writing a grammar, which can be turned
into a consumer of this library:
```
%package parser

// import some dependencies
%import "slices"
%import "bitbucket.org/dkolbly/fun"

// define some tokens
%token {lexeme} NAME INTEGER;
%token {string} NAME;
%token {int} INTEGER;
%token {lexeme} "+" "-";

// define the types for rules
%type {fun.Expr} start expr;

// define some rules
start ::= expr {}

expr ::= expr "+" expr {
    // return a composite thing
}

expr ::= n:NAME {
    return &fun.Ident{n}
}

expr ::= n:INTEGER {
    return &fun.Literal{n}
}

```

For a working example, consult the `example/` directory.

## Using the generator

The generator is the program that takes a metagrammar file and
produces Go code, the latter of which implements the grammar so
specified.

```
go install bitbucket.org/dkolbly/earley3/generate-earley3@latest
```

## Building using Bazel

I find the following `genrule` useful for building a grammar

```
genrule(
    name = "grammar_go",
    srcs = ["grammar.el"],
    outs = ["grammar.go"],
    cmd = "generate-earley3 $(location grammar.el) > \"$@\"",
)
```

then the generated grammar can be added added to the `go_library`
rule, as in:

```
load("@rules_go//go:def.bzl", "go_library")

go_library(
    name = "parser_lib",
    srcs = glob(["*.go"], exclude=["*_test.go"]) + [":grammar_go"],
    importpath = "bitbucket.org/dkolbly/some/thing",
    visibility = ["//visibility:public"],
    deps = [
        # ...
        "@org_bitbucket_dkolbly_earley3//:earley3",
    ]
)
```
