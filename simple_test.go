package earley

import (
	"context"
	"fmt"
	"io"
	"strconv"
	"testing"

	"bitbucket.org/dkolbly/logging"
)

func init() {
	logging.MustSetPrettyOutput(true, false)
}

type simpleScanner struct {
	tokens []string
}

type simpleLexeme string

type simpleTokenType int

const (
	simpleSymbol = simpleTokenType(iota)
	simpleString
	simpleDelim
	simpleNumber
	simpleOper
)

var stt = map[string]simpleTokenType{
	"SYM": simpleSymbol,
	"STR": simpleString,
	"DLM": simpleDelim,
	"OPR": simpleOper,
	"NUM": simpleNumber,
}

type classMatcher simpleTokenType

func (cm classMatcher) String() string {
	return simpleTokenType(cm).String()
}

func (cm classMatcher) Match(s Scope, l Lexeme) bool {
	return l.TokenType() == simpleTokenType(cm)
}

type operMatcher string

func (om operMatcher) Match(s Scope, l Lexeme) bool {
	return l.TokenType() == simpleOper && l.Value() == string(om)
}

func (om operMatcher) String() string {
	return fmt.Sprintf(`"%s"`, string(om))
}

type delimMatcher string

func (dm delimMatcher) Match(s Scope, l Lexeme) bool {
	return l.TokenType() == simpleDelim && l.Value() == string(dm)
}

func (dm delimMatcher) String() string {
	return fmt.Sprintf(`"%s"`, string(dm))
}

func (tt simpleTokenType) String() string {
	for k, v := range stt {
		if v == tt {
			return k
		}
	}
	panic("bad simple token type")
}

func (l simpleLexeme) Value() interface{} {
	if string(l)[:3] == "NUM" {
		n, _ := strconv.ParseInt(string(l)[4:], 10, 64)
		return n
	} else {
		return string(l)[4:]
	}
}

func (l simpleLexeme) TokenType() TokenTypeCode {
	if h, ok := stt[string(l)[:3]]; ok {
		return h
	}
	panic("bad test lexeme")
}

func (ss *simpleScanner) Scan(_ context.Context) (Lexeme, error) {
	if len(ss.tokens) == 0 {
		return nil, io.EOF
	}
	h := ss.tokens[0]
	ss.tokens = ss.tokens[1:]
	return simpleLexeme(h), nil
}

func TestParse(t *testing.T) {
	l := New(NonTerminal("expr"))

	expr := NonTerminal("expr")
	NUM := classMatcher(simpleNumber)
	PLUS := operMatcher("+")
	TIMES := operMatcher("*")
	OPEN := delimMatcher("(")
	CLOSE := delimMatcher(")")

	numValue := func(m []Meaning) Meaning {
		return m[0].(simpleLexeme).Value()
		//fmt.Sscanf(m[0].(simple "NUM %d",
		//m[0].strconv.ParseInt(m[0]
		//return int64(3)
	}

	addOp := func(m []Meaning) Meaning {
		return m[0].(int64) + m[2].(int64)
	}

	mulOp := func(m []Meaning) Meaning {
		return m[0].(int64) * m[2].(int64)
	}

	second := func(m []Meaning) Meaning {
		return m[1]
	}

	l.Rule(expr, NUM).Then(numValue)
	l.Rule(expr, expr, PLUS, expr).Then(addOp)
	l.Rule(expr, expr, TIMES, expr).Then(mulOp)
	l.Rule(expr, OPEN, expr, CLOSE).Then(second)

	src := &simpleScanner{
		tokens: []string{
			"NUM 10",
			"OPR +",
			"NUM 32",
		},
	}
	bg := context.Background()

	m, err := l.Parse(bg, src)
	if err != nil {
		t.Fatalf("failed to parse: %s", err)
	}
	if m.(int64) != 42 {
		t.Fatalf("expected 42, got %v", m)
	}
}
