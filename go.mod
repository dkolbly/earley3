module bitbucket.org/dkolbly/earley3

go 1.21

require (
	bitbucket.org/dkolbly/logging v0.9.4
	github.com/mattn/go-isatty v0.0.13 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
