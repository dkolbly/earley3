package earley

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"

	"bitbucket.org/dkolbly/logging"
)

var Trace = false

type TokenMatcher interface {
	Match(Scope, Lexeme) bool
	String() string
}

var ErrAmbiguous = errors.New("ambiguous")
var ErrNoParse = errors.New("no parse")

var log = logging.New("earley")

type Scope interface {
	Outer() Scope
	Context() context.Context
}

type grammarScope struct {
	outer Scope
	prods map[NonTerminal]*prodPair
}

func (s *grammarScope) Outer() Scope {
	return s.outer
}

func (s *grammarScope) Context() context.Context {
	return s.outer.Context()
}

type contextScope struct {
	outer Scope
	ctx   context.Context
}

func (s *contextScope) Outer() Scope {
	return s.outer
}

func (s *contextScope) Context() context.Context {
	return s.ctx
}

type prodPair struct {
	car *Production
	cdr *prodPair
}

func (p *prodPair) Len() int {
	i := 0
	for p != nil {
		p = p.cdr
		i++
	}
	return i
}

func Lookup(in Scope, nt NonTerminal) []*Production {
	var lst []*Production
	for p := lookup(in, nt); p != nil; p = p.cdr {
		lst = append(lst, p.car)
	}
	return lst
}

func lookup(in Scope, nt NonTerminal) *prodPair {
	for i := in; i != nil; i = i.Outer() {
		if Trace {
			log.Tracef(context.TODO(), "looking for %q in %p", nt, in)
		}
		if g, ok := i.(*grammarScope); ok {
			if p, ok := g.prods[nt]; ok {
				if Trace {
					log.Tracef(context.TODO(), "found %q in %p : %d prods", nt, i, p.Len())
				}
				return p
			}
		}
	}
	return nil
}

type Lang struct {
	defaultStart NonTerminal
	starts       map[NonTerminal]*Production
	global       Scope
}

type NonTerminal string

type Production struct {
	lhs     NonTerminal
	parts   []item
	reducer Reducer
	modes   []modality
	// short circuit operators
	accept bool
	reject bool
}

// Nth returns the nth item in the production, which is either a
// TokenMatcher or a NonTerminal
func (p *Production) Nth(i int) interface{} {
	if i < 0 {
		panic("negative index")
	}
	if i >= len(p.parts) {
		return nil
	}
	part := p.parts[i]
	if tmi, ok := part.(tokenMatcherItem); ok {
		return tmi.tm
	}
	if pi, ok := part.(productionItem); ok {
		return pi.nt
	}
	panic(fmt.Sprintf("invalid %T", part))
}

type item interface {
	String() string
	// could be either a NonTerminal or a TokenMatcher
}

type tokenMatcherItem struct {
	posn int
	tm   TokenMatcher
}

func (tmi tokenMatcherItem) String() string {
	return tmi.tm.String()
}

type productionItem struct {
	posn int
	nt   NonTerminal
}

func (pi productionItem) String() string {
	return string(pi.nt)
}

//func (tmi tokenMatcherItem)

type Item struct {
	Of *Production
	At int
}

func (l *Lang) Global() Scope {
	return l.global
}

func (l *Lang) Rule(lhs NonTerminal, formals ...interface{}) *Production {
	return l.global.(*grammarScope).Rule(lhs, formals...)
}

type GrammarScope interface {
	Scope
	Rule(lhs NonTerminal, formals ...interface{}) *Production
}

func NewGrammarScope(outer Scope) GrammarScope {
	return &grammarScope{
		outer: outer,
		prods: make(map[NonTerminal]*prodPair),
	}
}

func (s *grammarScope) Rule(lhs NonTerminal, formals ...interface{}) *Production {
	if Trace {
		log.Tracef(context.TODO(), "defining rule for %q in %p", lhs, s)
	}

	p := &Production{
		lhs: lhs,
	}

	for i, f := range formals {
		if tm, ok := f.(TokenMatcher); ok {
			p.parts = append(p.parts, tokenMatcherItem{
				posn: i,
				tm:   tm,
			})
			p.modes = append(p.modes, modeScan)
		} else if nt, ok := f.(NonTerminal); ok {
			p.parts = append(p.parts, productionItem{
				posn: i,
				nt:   nt,
			})
			p.modes = append(p.modes, modePredict)
		} else {
			panic(fmt.Sprintf("bad formal %T", f))
		}
	}
	p.modes = append(p.modes, modeComplete)
	s.prods[lhs] = &prodPair{
		car: p,
		cdr: lookup(s, lhs), // s.prods[lhs],
	}
	return p
}

func prodAppend(a, b *prodPair) *prodPair {
	if a == nil {
		return b
	} else {
		return &prodPair{
			car: a.car,
			cdr: prodAppend(a.cdr, b),
		}
	}
}

type Reducer func([]Meaning) Meaning

func defaultReducer(lst []Meaning) Meaning {
	return lst
}

func (p *Production) Then(r Reducer) *Production {
	p.reducer = r
	return p
}

// setting this on a production causes acceptance of the input which
// this production is reduced, and the meaning of the accepted
// production is the meaning of the entire parse
func (p *Production) Accept() *Production {
	p.accept = true
	return p
}

func (p *Production) Reject() *Production {
	p.reject = true
	return p
}

func New(starts ...NonTerminal) *Lang {
	init := make(map[NonTerminal]*Production)

	for _, start := range starts {
		init[start] = &Production{
			lhs:   NonTerminal("$input"),
			parts: []item{productionItem{posn: 0, nt: start}},
			modes: []modality{modePredict, modeComplete},
		}
	}

	return &Lang{
		defaultStart: starts[0],
		starts:       init,
		global: &grammarScope{
			prods: make(map[NonTerminal]*prodPair),
		},
	}
}

// Extend takes a given language, extends it with the language
// (productions) available from the other language; TODO this API
// could be improved.  The Lang object is fuzzy in its semantics; we
// should be doing this with scope instead.
func (l *Lang) Extend(with *Lang) *Lang {

	flat := make(map[NonTerminal]*prodPair)

	g := l.global.(*grammarScope)
	for k, v := range g.prods {
		flat[k] = v
	}
	for k, v := range with.global.(*grammarScope).prods {
		fmt.Printf("extending with %q...\n", k)
		flat[k] = prodAppend(v, flat[k])
	}

	return &Lang{
		defaultStart: l.defaultStart,
		starts:       l.starts,
		global: &grammarScope{
			prods: flat,
		},
	}
}

func (l *Lang) Restart(start NonTerminal) *Lang {
	starts := make(map[NonTerminal]*Production)
	starts[start] = &Production{
		lhs:   NonTerminal("$input"),
		parts: []item{productionItem{posn: 0, nt: start}},
		modes: []modality{modePredict, modeComplete},
	}

	return &Lang{
		defaultStart: start,
		starts:       starts,
		global:       l.global,
	}
}

type Meaning interface{}

type parse struct {
	active    []*tuple
	scan      []*tuple
	completed []*tuple
	final     []Meaning
	vacuous   []NonTerminal
	accepted  bool
}

type tupleKey struct {
	prod *Production
	posn int
	from *parse // which parse state introduced this tuple?
	ctx  Scope  // what is the scope associated with this tuple?
}

func (t *tuple) next() item {
	return t.key.prod.parts[t.key.posn]
}

type modality int

const (
	modePredict = modality(iota)
	modeScan
	modeComplete
)

func (tk tupleKey) modality() modality {
	return tk.prod.modes[tk.posn]
}

/*func (tk tupleKey) next() tupleKey {
	return tupleKey{
		prod: tk.prod,
		posn: tk.posn + 1,
	}
}*/

func (tk tupleKey) String() string {
	buf := &bytes.Buffer{}
	any := false
	space := func() {
		if any {
			buf.WriteByte(' ')
		}
		any = true
	}

	for i, part := range tk.prod.parts {
		if tk.posn == i {
			space()
			buf.WriteRune('·')
		}
		space()
		buf.WriteString(part.String())
	}
	if tk.posn == len(tk.prod.parts) {
		space()
		buf.WriteRune('·')
	}
	return buf.String()
}

type meaningPair struct {
	value Meaning
	next  *meaningPair
}

func (l *meaningPair) length() int {
	n := 0
	for l != nil {
		n++
		l = l.next
	}
	return n
}

func cons(car Meaning, cdr *meaningPair) *meaningPair {
	return &meaningPair{
		value: car,
		next:  cdr,
	}
}

type tuple struct {
	key   tupleKey
	accum *meaningPair
}

type visitSet map[tupleKey]struct{}

func (p *parse) visit(ctx context.Context, seen visitSet, t *tuple) error {
	if _, ok := seen[t.key]; ok {
		if Trace {
			log.Debugf(ctx, "  ; but already saw %s", t.key)
		}
		// already seen
		return nil
	}
	seen[t.key] = struct{}{}

	switch t.key.modality() {
	case modeComplete:
		p.completed = append(p.completed, t)
		return p.complete(ctx, seen, t)

	case modeScan:
		// these will be picked up later
		p.scan = append(p.scan, t)
		return nil

	case modePredict:
		p.active = append(p.active, t)
		if Trace {
			log.Debugf(ctx, "need to predict :: %s", t.key)
		}
		return p.predict(ctx, seen, t)

	default:
		panic("invalid key mode")
	}

}

func (l *Lang) Parse(ctx context.Context, src Scanner) (Meaning, error) {
	return l.ParseStart(ctx, src, l.defaultStart)
}

func (l *Lang) NewLangWithGlobalScope(s Scope) *Lang {
	return &Lang{
		defaultStart: l.defaultStart,
		starts:       l.starts,
		global:       s,
	}
}

func (l *Lang) ParseStart(ctx context.Context, src Scanner, start NonTerminal) (Meaning, error) {
	var s *Production

	s, ok := l.starts[start]
	if !ok {
		return nil, fmt.Errorf("non-terminal %q is not a start symbol", start)
	}

	// bootstrap
	boot := &tuple{
		key: tupleKey{
			prod: s,
			posn: 0,
			ctx: &contextScope{
				outer: l.global,
				ctx:   ctx,
			},
		},
	}
	seen := make(visitSet)

	p := &parse{}
	err := p.visit(ctx, seen, boot)
	if err != nil {
		return nil, err
	}

	i := 0
	for {
		var err error
		//p.debug(ctx, i)
		prev := p
		var lex Lexeme
		p, lex, err = p.step(ctx, src)
		if errors.Is(err, io.EOF) || (p != nil && p.accepted) {
			if p.accepted {
				log.Debugf(ctx, "early acceptance")
			}
			if len(p.final) == 1 {
				return p.final[0], nil
			} else if len(p.final) > 1 {
				return nil, ErrAmbiguous
			} else {
				return nil, ErrNoParse
			}
		} else if err != nil {
			log.Debugf(ctx, "choked: %s", err)
			return nil, err
		} else if p == nil {
			/*
				// this hack is totally jacked up; we really need
				// to loop around and make sure that we are matching EOF
				log.Debugf(ctx, "can't scan, %d final", len(prev.final))
				if len(prev.final) == 1 {
					return prev.final[0], nil
				} else if len(prev.final) > 1 {
					return nil, ErrAmbiguous
				}
			*/
			return nil, prev.cantscan(lex)
		}
		i++
	}
}

func (p *parse) step(ctx context.Context, src Scanner) (*parse, Lexeme, error) {
	lex, err := src.Scan(ctx)
	if err != nil {
		return p, lex, err
	}

	seen := make(visitSet)
	next := &parse{}
	if Trace {
		log.Debugf(ctx, "%d scannable", len(p.scan))
	}

	for _, check := range p.scan {
		if m := check.advancePast(check.key.ctx, lex); m != nil {
			nt, err := check.advance(m)
			if err != nil {
				return nil, lex, err
			}
			if Trace {
				log.Debugf(ctx, "advanced past %s to :: %s",
					lex,
					nt.key)
			}
			err = next.visit(ctx, seen, nt)
			if err != nil {
				return nil, lex, err
			}
		}
	}
	if len(next.scan) == 0 && len(next.final) == 0 {
		if len(next.vacuous) > 0 {
			return nil, lex, next.cantpredict(p)
		}
		return nil, lex, nil
	}

	return next, lex, nil
}

type ScopeChanger interface {
	ScopeChange(Scope) Scope
}

type ScopeChangerWithErr interface {
	ScopeChange(Scope) (Scope, error)
}

func (t *tuple) advance(m Meaning) (*tuple, error) {
	ctx := t.key.ctx
	// if the meaning that we're pushing is also a scope changer,
	// then update our scope context
	if chg, ok := m.(ScopeChanger); ok {
		if Trace {
			log.Debugf(ctx.Context(), "changing scope! %T", chg)
		}
		ctx = chg.ScopeChange(ctx)
		if Trace {
			log.Debugf(ctx.Context(), "new scope is %T %p", ctx, ctx)
		}
	}
	if chg, ok := m.(ScopeChangerWithErr); ok {
		if Trace {
			log.Debugf(ctx.Context(), "changing scope! %T", chg)
		}
		var err error
		ctx, err = chg.ScopeChange(ctx)
		if err != nil {
			return nil, err
		}
		if Trace {
			log.Debugf(ctx.Context(), "new scope is %T %p", ctx, ctx)
		}
	}

	nk := tupleKey{
		prod: t.key.prod,
		posn: t.key.posn + 1,
		from: t.key.from,
		ctx:  ctx,
	}
	return &tuple{
		key:   nk,
		accum: cons(m, t.accum),
	}, nil
}

func (p *parse) predict(ctx context.Context, seen visitSet, t *tuple) error {
	//i := t.key.posn
	next := t.next().(productionItem)

	count := 0
	for lst := lookup(t.key.ctx, next.nt); lst != nil; lst = lst.cdr {
		nt := &tuple{
			key: tupleKey{
				prod: lst.car,
				posn: 0,
				from: p,
				ctx:  t.key.ctx,
			},
		}
		if Trace {
			log.Debugf(ctx, "consider %s", nt.key)
		}
		err := p.visit(ctx, seen, nt)
		if err != nil {
			return err
		}
		count++
	}
	if count == 0 {
		// this is purely for debugging grammars, so that
		// if we get an error we can mention vacuous non terminals
		p.vacuous = append(p.vacuous, next.nt)
	}
	return nil
}

func (p *parse) complete(ctx context.Context, seen visitSet, t *tuple) error {
	nt := t.key.prod.lhs
	if Trace {
		log.Debugf(ctx, "completing %s ⇒ %s", nt, t.key)
	}

	if t.key.from == nil {
		if Trace {
			log.Debugf(ctx, "special case: completing bootstrap production")
		}
		if t.accum.length() != 1 {
			panic("bootstrap production doesn't have 1 meaning")
		}
		p.final = append(p.final, t.accum.value)
		return nil
	}

	n := t.accum.length()
	mlist := make([]Meaning, n)
	i := n
	l := t.accum
	for l != nil {
		i--
		mlist[i] = l.value
		l = l.next
	}

	red := t.key.prod.reducer
	if red == nil {
		red = defaultReducer
	}

	means := red(mlist)

	if t.key.prod.accept {
		p.final = append(p.final, means)
		p.accepted = true
		return nil
	}

	if t.key.from != nil {
		for _, a := range t.key.from.active {
			if a.nextIsForNonTerminal(nt) {
				next, err := a.advance(means)
				if err != nil {
					return err
				}
				err = p.visit(ctx, seen, next)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (t *tuple) nextIsForNonTerminal(nt NonTerminal) bool {
	return t.next().(productionItem).nt == nt
}

func (t *tuple) advancePast(ctx Scope, lex Lexeme) Meaning {
	next := t.key.prod.parts[t.key.posn]
	if tmi, ok := next.(tokenMatcherItem); ok {
		if tmi.tm.Match(ctx, lex) {
			return lex
		} else {
			return nil
		}
	} else {
		return nil
	}
}

type ParseError struct {
	in   *parse
	prev *parse
	saw  Lexeme
}

func (err *ParseError) Error() string {
	buf := &bytes.Buffer{}

	scans := err.in.scan
	if err.prev != nil {
		fmt.Fprintf(buf, "Was just scanning past:\n")
		scans = err.prev.scan
	} else {
		fmt.Fprintf(buf, "Expected one of these:\n")
	}
	for _, t := range scans {
		fmt.Fprintf(buf, "   %s -> %s\n", t.key.prod.lhs, t.key)
	}
	if err.saw != nil {
		fmt.Fprintf(buf, "but saw: %s", err.saw)
	}

	if len(err.in.vacuous) > 0 {
		fmt.Fprintf(buf, "\nand there were non-terminals with no rules defined:\n")
		for _, n := range err.in.vacuous {
			fmt.Fprintf(buf, "    %s\n", n)
		}
	}
	if err.in != nil {
		fmt.Fprintf(buf, "\nthe current state is:\n")
		buf.WriteString(err.in.debugstr())
	}
	return buf.String()
}

func (p *parse) cantpredict(prev *parse) error {
	return &ParseError{
		in:   p,
		prev: prev,
	}
}

func (p *parse) cantscan(lex Lexeme) error {
	return &ParseError{
		in:  p,
		saw: lex,
	}
}

func (p *parse) debug(ctx context.Context, i int) {
	log.Debugf(ctx, "=================================[ %d ]===\n%s", i,
		p.debugstr())
}

func (p *parse) debugstr() string {

	buf := &bytes.Buffer{}

	fmt.Fprintf(buf, "%d active:\n", len(p.active))
	for j, t := range p.active {
		fmt.Fprintf(buf, "   (%d) %s ⇒ %s\n", j, t.key.prod.lhs, t.key)
	}

	fmt.Fprintf(buf, "%d scannable:\n", len(p.scan))
	for j, t := range p.scan {
		fmt.Fprintf(buf, "   (%d) %s ⇒ %s\n", j, t.key.prod.lhs, t.key)
	}

	fmt.Fprintf(buf, "%d complete:\n", len(p.completed))
	for j, t := range p.completed {
		fmt.Fprintf(buf, "   (%d) %s ⇒ %s\n", j, t.key.prod.lhs, t.key)
	}

	return buf.String()
}
