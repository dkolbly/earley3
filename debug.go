package earley

import (
	"fmt"
	"io"
	"sort"
)

func (s *Lang) WriteTo(dst io.Writer) {
	fmt.Fprintf(dst, "start: %s\n", s.defaultStart)

	var nts []string
	gs := s.global.(*grammarScope)
	for k := range gs.prods {
		nts = append(nts, string(k))
	}

	sort.Strings(nts)

	for _, k := range nts {
		v := gs.prods[NonTerminal(k)]
		fmt.Fprintf(dst, "  %s ::=\n", k)
		for pp := v; pp != nil; pp = pp.cdr {
			if len(pp.car.parts) == 0 {
				fmt.Fprintf(dst, "     | ε\n")
			} else {
				fmt.Fprintf(dst, "     |")
				for _, part := range pp.car.parts {
					fmt.Fprintf(dst, " %s", part)
				}
				fmt.Fprintf(dst, "\n")
			}
		}
	}
}

func WriteGrammar(dst io.Writer, s Scope) {
	covered := make(map[string]bool)

	for s != nil {
		if gs, ok := s.(*grammarScope); ok {
			fmt.Fprintf(dst, "SCOPE: === Grammar === %p\n", s)

			var nts []string
			for k := range gs.prods {
				nts = append(nts, string(k))
			}

			sort.Strings(nts)

			for _, k := range nts {
				if covered[k] {
					fmt.Fprintf(dst, "  %s ::= *overridden*\n", k)
					continue
				}
				covered[k] = true
				v := gs.prods[NonTerminal(k)]
				fmt.Fprintf(dst, "  %s ::=\n", k)
				for pp := v; pp != nil; pp = pp.cdr {
					if len(pp.car.parts) == 0 {
						fmt.Fprintf(dst, "     | ε\n")
					} else {
						fmt.Fprintf(dst, "     |")
						for _, part := range pp.car.parts {
							fmt.Fprintf(dst, " %s", part)
						}
						fmt.Fprintf(dst, "\n")
					}
				}
			}

		} else {
			fmt.Fprintf(dst, "SCOPE: %T %p\n", s, s)
		}
		s = s.Outer()
	}
}
