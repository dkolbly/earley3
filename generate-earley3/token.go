package main

import (
	"bytes"
	"fmt"
	"regexp"

	e "bitbucket.org/dkolbly/earley3"
)

type TokenType int

const (
	Symbol = TokenType(iota)
	String
	Define         // ::=
	Colon          // :
	Semicolon      // ;
	Block          // { .... }
	Dot            // .
	TokenKeyword   // %token
	TypeKeyword    // %type
	PackageKeyword // %package
	ImportKeyword  // %import
	ExportKeyword  // %export (used to mark NTs for export)
	AcceptKeyword  // %accept
	StartKeyword   // %start
)

// String implements earley.TokenMatcher
func (tt TokenType) String() string {
	switch tt {
	case Symbol:
		return "SYMBOL"
	case String:
		return "STRING"
	case Define:
		return "DEFINE"
	case Colon:
		return "COLON"
	case Semicolon:
		return "SEMICOLON"
	case Block:
		return "BLOCK"
	case TokenKeyword:
		return "%token"
	case PackageKeyword:
		return "%package"
	case ImportKeyword:
		return "%import"
	case TypeKeyword:
		return "%type"
	case ExportKeyword:
		return "%export"
	case AcceptKeyword:
		return "%accept"
	case StartKeyword:
		return "%start"
	}
	panic("what?!?")
}

// Match implements earley.TokenMatcher; this allows the token type
// constants to be used as items on the RHS of our own grammar rules
func (tt TokenType) Match(_ e.Scope, l e.Lexeme) bool {
	return l.(Token).Type == tt
}

type Token struct {
	Type  TokenType
	File  string
	Line  int
	Datum interface{}
}

func (t Token) String() string {
	return fmt.Sprintf("%d:%s:%v", t.Line, t.Type, t.Datum)
}

func (t Token) Value() interface{} {
	return t.Datum
}

var symbolPattern = regexp.MustCompile(`^[A-Za-z_][A-Za-z0-9_]*`)
var definePattern = regexp.MustCompile(`^::=`)

type charclass byte

const (
	invalid = charclass(iota)
	space
	nl
	id1
	colon
	semicolon
	brace
	quote
	keyword
	slash
	dot
)

var class = [128]charclass{
	' ':  space,
	'\t': space,
	'\n': nl,
	':':  colon,
	';':  semicolon,
	'{':  brace,
	'"':  quote,
	'%':  keyword, // all keywords in our metagrammar are introduced with %
	'/':  slash,
	'_':  id1,
	'.':  dot,
	'a':  id1,
	'b':  id1,
	'c':  id1,
	'd':  id1,
	'e':  id1,
	'f':  id1,
	'g':  id1,
	'h':  id1,
	'i':  id1,
	'j':  id1,
	'k':  id1,
	'l':  id1,
	'm':  id1,
	'n':  id1,
	'o':  id1,
	'p':  id1,
	'q':  id1,
	'r':  id1,
	's':  id1,
	't':  id1,
	'u':  id1,
	'v':  id1,
	'w':  id1,
	'x':  id1,
	'y':  id1,
	'A':  id1,
	'B':  id1,
	'C':  id1,
	'D':  id1,
	'E':  id1,
	'F':  id1,
	'G':  id1,
	'H':  id1,
	'I':  id1,
	'J':  id1,
	'K':  id1,
	'L':  id1,
	'M':  id1,
	'N':  id1,
	'O':  id1,
	'P':  id1,
	'Q':  id1,
	'R':  id1,
	'S':  id1,
	'T':  id1,
	'U':  id1,
	'V':  id1,
	'W':  id1,
	'X':  id1,
	'Y':  id1,
}

func scan(file string, src []byte) ([]Token, error) {
	var lst []Token

	line := 1
	push := func(t TokenType, v interface{}) {
		lst = append(lst, Token{
			File:  file,
			Type:  t,
			Line:  line,
			Datum: v,
		})
	}

	prev := src

	for len(src) > 0 {
		switch class[src[0]] {
		case space:
			if src[0] == '\n' {
				line++
			}
			src = src[1:]
		case nl:
			line++
			src = src[1:]
		case id1:
			m := symbolPattern.Find(src)
			if m == nil {
				panic(fmt.Sprintf("charclass fail %.30q?", src))
			}
			push(Symbol, string(m))
			src = src[len(m):]
		case colon:
			if m := definePattern.Find(src); m != nil {
				push(Define, nil)
				src = src[len(m):]
			} else {
				push(Colon, nil)
				src = src[1:]
			}
		case semicolon:
			push(Semicolon, nil)
			src = src[1:]
		case dot:
			push(Dot, nil)
			src = src[1:]
		case brace:
			var err error
			var data []byte
			data, line, src, err = scanblock(line, src)
			if err != nil {
				return nil, err
			}
			push(Block, data)

		case quote:
			var err error
			var data string
			data, src, err = scanstring(line, src)
			if err != nil {
				return nil, err
			}
			push(String, data)

		case keyword:
			m := symbolPattern.Find(src[1:])
			if m == nil {
				return nil, fmt.Errorf("invalid use of % on line %d", line)
			}
			src = src[1+len(m):]
			switch string(m) {
			case "token":
				push(TokenKeyword, nil)
			case "type":
				push(TypeKeyword, nil)
			case "package":
				push(PackageKeyword, nil)
			case "import":
				push(ImportKeyword, nil)
			case "export":
				push(ExportKeyword, nil)
			case "accept": // a short-circuit operator
				push(AcceptKeyword, nil)
			case "start": // multiple start symbols
				push(StartKeyword, nil)
			default:
				return nil, fmt.Errorf("invalid keyword on line %d", line)
			}
		case slash:
			if len(src) < 2 {
				return nil, fmt.Errorf("bare \"/\" at end on line %d", line)
			}
			if src[1] != '/' {
				return nil, fmt.Errorf("invalid \"/\" on line %d", line)
			}
			for len(src) > 0 && src[0] != '\n' {
				src = src[1:]
			}

		default:
			return nil, fmt.Errorf("invalid char %q on line %d", src[0], line)
		}
		if len(src) == len(prev) {
			panic("empty token")
		}
		prev = src
	}
	return lst, nil
}

func scanblock(lineStart int, src []byte) ([]byte, int, []byte, error) {
	line := lineStart
	depth := 0

	i := 0

	for {
		if i >= len(src) {
			return nil, 0, nil, fmt.Errorf("unterminated block started at line %d", lineStart)
		}

		ch := src[i]
		i++

		switch ch {
		case '{':
			depth++
		case '}':
			depth--
			if depth == 0 {
				return src[:i], line, src[i:], nil
			}
		case '\n':
			line++
		}
	}
}

func scanstring(line int, src []byte) (string, []byte, error) {
	var buf bytes.Buffer

	i := 1 // skip initial quote
	esc := false

	for {
		if i >= len(src) || src[i] == '\n' {
			return "", nil, fmt.Errorf("unterminated string on line %d", line)
		}

		ch := src[i]
		i++

		if esc {
			switch ch {
			case 'n':
				buf.WriteByte('\n')
			case '\\':
				buf.WriteByte('\\')
			default:
				return "", nil, fmt.Errorf("unknown escape sequence %q on line %d", ch)
			}
			esc = false
		} else {
			switch ch {
			case '\\':
				esc = true
			case '"':
				return buf.String(), src[i:], nil
			default:
				buf.WriteByte(ch)
			}
		}
	}
}
