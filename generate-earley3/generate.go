package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	//"path/filepath"
	"sort"
	"strings"

	e "bitbucket.org/dkolbly/earley3"
)

// TODO use go/format.Source() to generate go fmt'd output

func main() {
	var output = flag.String("o", "", "output file")
	var cons = flag.String("cons", "Lang", "constructor function name")
	flag.Parse()
	generatefor(*output, *cons, flag.Args())
}

type scanner struct {
	tokens []Token
}

func (s *scanner) Scan(_ context.Context) (e.Lexeme, error) {
	if len(s.tokens) == 0 {
		return nil, io.EOF
	}
	next := s.tokens[0]
	s.tokens = s.tokens[1:]
	return next, nil
}

func generatefor(dst, consname string, sources []string) {

	var alldecls []Decl

	for _, src := range sources {
		buf, err := ioutil.ReadFile(src)
		if err != nil {
			panic(err)
		}
		lst, err := scan(src, buf)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to scan: %s\n", err)
			os.Exit(1)
		}
		if false {
			for i, t := range lst {
				fmt.Printf("[%d] %#v\n", i, t)
			}
		}
		g := makeGrammar()

		ctx := context.Background()
		result, err := g.Parse(ctx, &scanner{lst})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to parse:\n%s\n", err)
			os.Exit(1)
		}
		decls := result.([]Decl)
		alldecls = append(alldecls, decls...)
	}

	gbuilder := &bytes.Buffer{}
	post := &bytes.Buffer{}

	job := &job{
		packageName:     "main",
		constructorName: consname,
		dst:             gbuilder,
		funcs:           post,
		littokens:       make(map[string]*tokeninfo),
		tokens:          make(map[string]*tokeninfo),
		ntindex:         make(map[string]int),
		rulex:           make(map[*Rule]int),
		nts:             make(map[string]*ntinfo),
		used:            make(map[string]struct{}),
		starts: map[string]int{
			"start": 0,
		},
	}

	job.use(job.intern("start")) // this happens to arrange for
	// start to be nt0

	// preprocess all the decls
	for _, decl := range alldecls {
		decl.process(job, 0) // phase[0]
	}

	// all the starts have been elaborated
	for _, k := range sortedKeysInt(job.starts) {
		job.use(job.intern(k))
	}

	for _, decl := range alldecls {
		decl.process(job, 1) // phase[1]
	}

	// generate the code
	for _, decl := range alldecls {
		if r, ok := decl.(*Rule); ok {
			r.render(job)
		}
	}

	job.dst = os.Stdout
	if dst != "" {
		f, err := os.Create(dst)
		if err != nil {
			panic(err)
		}
		job.dst = f
		defer f.Close()
	}

	job.preamble()
	job.dst.Write(gbuilder.Bytes())
	job.postamble()

	job.dst.Write(post.Bytes())
}

func unbrace(s string) string {
	if s == "" {
		return ""
	}

	if s[0] != '{' || s[len(s)-1] != '}' {
		panic("was not braced to start with?!?")
	}
	return s[1 : len(s)-1]
}

func (j *job) use(n string) {
	j.used[n] = struct{}{}
}

func (j *job) preamble() {
	j.imports = append(j.imports, &ImportDef{Package: "fmt"})
	j.imports = append(j.imports, &ImportDef{As: "e", Package: "bitbucket.org/dkolbly/earley3"})

	dst := j.dst

	fmt.Fprintf(dst, "package %s\n", j.packageName)
	fmt.Fprintf(dst, "\n")
	fmt.Fprintf(dst, "import (\n")
	for _, imp := range j.imports {
		if imp.As == "" {
			fmt.Fprintf(dst, "\t%q\n", imp.Package)
		} else {
			fmt.Fprintf(dst, "\t%s %q\n", imp.As, imp.Package)
		}
	}
	fmt.Fprintf(dst, ")\n")
	fmt.Fprintf(dst, "func %s(tokens map[string]e.TokenMatcher) *e.Lang {\n", j.constructorName)

	// bind nonterminals (but sort them so we don't spuriously
	// trash the generated file)

	for _, k := range sortedKeysInt(j.ntindex) {
		n := j.intern(k)
		if _, ok := j.used[n]; ok {
			fmt.Fprintf(dst, "\t%s := e.NonTerminal(%q)\n",
				j.intern(k),
				k)
		}
	}

	fmt.Fprintf(dst, "\tmusttoken := func(n string) e.TokenMatcher {\n")
	fmt.Fprintf(dst, "\t\ttm := tokens[n]\n")
	fmt.Fprintf(dst, "\t\tif tm == nil {\n")
	fmt.Fprintf(dst, "\t\t\tpanic(fmt.Sprintf(\"token matcher for %%q not provided\", n))\n")
	fmt.Fprintf(dst, "\t\t}\n")
	fmt.Fprintf(dst, "\t\treturn tm\n")
	fmt.Fprintf(dst, "\t}\n")

	// bind literal tokens
	for _, k := range sortedKeysTokeninfo(j.littokens) {
		n := j.termid(k)
		if _, ok := j.used[n]; ok {
			fmt.Fprintf(dst, "\t%s := musttoken(%q)\n", n, k)
		}
	}

	// bind token classes
	for _, k := range sortedKeysTokeninfo(j.tokens) {
		n := j.termid(k)
		if _, ok := j.used[n]; ok {
			fmt.Fprintf(dst, "\t%s := musttoken(%q)\n", n, k)
		}
	}

	// we are a little opinionated; we require "start" to be the
	// name of the start symbol

	fmt.Fprintf(dst, "\t// init grammar with start symbols\n")
	fmt.Fprintf(dst, "\tg := e.New(\n")
	fmt.Fprintf(dst, "\t\t%s, // start\n", j.intern("start"))
	for k := range j.starts {
		if k != "start" {
			fmt.Fprintf(dst, "\t\t%s, // %s\n", j.intern(k), k)
		}
	}
	fmt.Fprintf(dst, "\t\t)\n")
}

func (j *job) postamble() {
	dst := j.dst
	fmt.Fprintf(dst, "\treturn g\n")
	fmt.Fprintf(dst, "}\n")
}

func (td *PackageDef) process(j *job, phase int) {
	if phase != 0 {
		return
	}
	j.packageName = td.Name
}

func (td *StartDef) process(j *job, phase int) {
	if phase != 0 {
		return
	}
	for _, n := range td.Symbols {
		j.starts[n] = td.Line
	}
}

// note that this is identical to what we do for StartDef; the
// difference semantically is that we don't intend these to be start
// symbols per se

func (td *ExportDef) process(j *job, phase int) {
	if phase != 0 {
		return
	}
	for _, n := range td.Symbols {
		j.starts[n] = td.Line
	}
}

func (td *TypeDef) process(j *job, phase int) {
	if phase != 0 {
		return
	}
	if td.LiteralToken {
		for _, sym := range td.Symbols {
			j.littokens[sym] = &tokeninfo{
				literal: sym,
				id:      len(j.littokens) + len(j.tokens),
				gotype:  unbrace(td.TypeSpec),
			}
		}
	} else if td.TokenClass {
		for _, sym := range td.Symbols {
			j.tokens[sym] = &tokeninfo{
				literal: sym,
				id:      len(j.littokens) + len(j.tokens),
				gotype:  unbrace(td.TypeSpec),
			}
		}
	} else {
		for _, sym := range td.Symbols {
			j.nts[sym] = &ntinfo{
				gotype: unbrace(td.TypeSpec),
			}
		}
	}
}

type tokeninfo struct {
	literal string // for literal tokens like ";"
	gotype  string // the go type returned by the lexeme's Value()
	id      int
}

type ntinfo struct {
	gotype string
}

type job struct {
	packageName     string
	imports         []*ImportDef
	constructorName string
	dst             io.Writer
	funcs           io.Writer
	used            map[string]struct{} // used variables
	rulex           map[*Rule]int
	tokens          map[string]*tokeninfo // token classes
	littokens       map[string]*tokeninfo
	ntindex         map[string]int
	tfinal          map[string]string
	nts             map[string]*ntinfo
	starts          map[string]int // where declared?
}

func (j *job) ntgotype(nt string) string {
	if i, ok := j.nts[nt]; ok {
		return i.gotype
	}
	return ""
}

func (j *job) terminfo(t string) *tokeninfo {
	if ti, ok := j.littokens[t]; ok {
		return ti
	}
	if ti, ok := j.tokens[t]; ok {
		return ti
	}
	return nil
}

func (j *job) termid(t string) string {
	if ti, ok := j.littokens[t]; ok {
		return fmt.Sprintf("tl%d", ti.id)
	}
	if ti, ok := j.tokens[t]; ok {
		return fmt.Sprintf("tc%d", ti.id)
	}
	return ""
}

func (j *job) rulereducer(r *Rule) string {
	return fmt.Sprintf("_%sreduce_%d", j.constructorName, j.ruleid(r))
}

func (j *job) ruleid(r *Rule) int {
	id, ok := j.rulex[r]
	if !ok {
		id = len(j.rulex)
		//fmt.Fprintf(os.Stderr, "assigning [%d] for %s\n", id, r.Left)
		j.rulex[r] = id
	}
	return id
}

func (j *job) intern(nt string) string {
	id, ok := j.ntindex[nt]
	if !ok {
		id = len(j.ntindex)
		j.ntindex[nt] = id
	}
	return fmt.Sprintf("nt%d", id)
}

func (r *Rule) process(j *job, phase int) {
	if phase != 1 {
		return
	}
	for i, item := range r.Right {
		if item.Symbolic == "" && item.Terminal == "" {
			panic(fmt.Sprintf("~line %d: rhs[%d] is neither symbolic nor terminal",
				i, r.Line))
		}

		if item.Symbolic != "" {
			if tid := j.termid(item.Symbolic); tid != "" {
				j.use(tid)
			} else {
				j.use(j.intern(item.Symbolic))
			}
		} else {
			tid := j.termid(item.Terminal)
			if tid == "" {
				panic(fmt.Sprintf("~line %d: [%d] token %q is not defined",
					r.Line, i, item.Terminal))
			}
			j.use(tid)
		}
	}
}

func (imp *ImportDef) process(j *job, phase int) {
	if phase != 0 {
		return
	}
	j.imports = append(j.imports, imp)
}

func (r *Rule) render(j *job) {
	dst := j.dst

	if _, ok := j.used[j.intern(r.Left)]; !ok {
		fmt.Fprintf(os.Stderr, "%s is never used, not outputting rule\n", r.Left)
		// the LHS is never used; discard this entire rule
		return
	}

	comment := func(dst io.Writer, indent string) {
		fmt.Fprintf(dst, "%s// %s, line %d\n", indent, r.File, r.Line)
		fmt.Fprintf(dst, "%s//\n", indent)
		fmt.Fprintf(dst, "%s//  %s ::=\n", indent, r.Left)
		for _, item := range r.Right {
			if item.Symbolic != "" {
				fmt.Fprintf(dst, "%s//        %s\n", indent, item.Symbolic)
			} else {
				fmt.Fprintf(dst, "%s//        %q\n", indent, item.Terminal)
			}
		}
		if r.Accept {
			fmt.Fprintf(dst, "%s//        %%accept\n", indent)
		}

		fmt.Fprintf(dst, "%s//\n", indent)
	}

	comment(dst, "\t")
	fmt.Fprintf(dst, "\tg.Rule(%s /* ::= */", j.intern(r.Left))
	for i, item := range r.Right {
		if item.Symbolic != "" {
			if tid := j.termid(item.Symbolic); tid != "" {
				fmt.Fprintf(dst, ", %s", tid)
			} else {
				ntid := j.intern(item.Symbolic)
				fmt.Fprintf(dst, ", %s", ntid)
			}
		} else {
			tid := j.termid(item.Terminal)
			if tid == "" {
				panic(fmt.Sprintf("~line %d: [%d] token %q is not defined",
					r.Line, i, item.Terminal))
			}
			fmt.Fprintf(dst, ", %s", tid)
		}
	}
	fmt.Fprintf(dst, ").\n")
	if r.Accept {
		fmt.Fprintf(dst, "\t\tAccept().\n")
	}
	fmt.Fprintf(dst, "\t\tThen(func(_m []e.Meaning) e.Meaning {\n")
	var args []string
	var argTypes []string

	for i, item := range r.Right {
		name := item.Name
		if item.Symbolic != "" {
			if name == "" {
				name = item.Symbolic
			}
			var t string

			ti := j.terminfo(item.Symbolic)
			if ti != nil {
				t = ti.gotype
			} else {
				t = j.ntgotype(item.Symbolic)
			}
			if t != "" {
				fmt.Fprintf(dst, "\t\t\t%s := _m[%d].(%s)\n",
					name,
					i,
					t,
				)
				args = append(args, name)
				argTypes = append(argTypes, t)
			} else {
				fmt.Fprintf(dst, "\t\t\t// (%s has no type decl)\n",
					item.Symbolic)

				// but yet the grammar demands a type
				if item.Name != "" {
					panic(fmt.Sprintf("~line %d: %q has no type",
						r.Line, item.Symbolic))
				}
			}
		} else {
			// only bind terminals if they have names
			if name != "" {
				ti := j.terminfo(item.Terminal)
				if ti == nil {
					fmt.Fprintf(os.Stderr, "terminal %q has no type\n", item.Terminal)
					continue
				}
				t := ti.gotype
				fmt.Fprintf(dst, "\t\t\t%s := _m[%d].(%s)\n",
					name,
					i,
					t,
				)
				args = append(args, name)
				argTypes = append(argTypes, t)
			}
		}
	}
	fmt.Fprintf(dst, "\t\t\treturn %s(%s)\n",
		j.rulereducer(r),
		strings.Join(args, ", "))
	fmt.Fprintf(dst, "\t\t})\n")

	dst = j.funcs

	fmt.Fprintf(dst, "\n")
	fmt.Fprintf(dst, "//--------------------------------------\n")
	comment(dst, "")

	fmt.Fprintf(dst, "//line %s:%d\n", r.File, r.Line)
	fmt.Fprintf(dst, "func %s(", j.rulereducer(r))
	for i, arg := range args {
		if i > 0 {
			fmt.Fprintf(dst, ", ")
		}
		fmt.Fprintf(dst, "%s %s", arg, argTypes[i])
	}
	fmt.Fprintf(dst, ") %s ", j.ntgotype(r.Left))

	if len(r.Action.Code) == 2 && string(r.Action.Code) == "{}" {
		fmt.Fprintf(dst, "{\n")
		fmt.Fprintf(dst, "\t// synthesized body\n")
		if len(args) == 0 {
			panic(fmt.Sprintf("~line %d: cannot synthesize body; no args with known types, and empty body",
				r.Line))
		}
		fmt.Fprintf(dst, "\treturn %s\n", args[0])
		fmt.Fprintf(dst, "}\n")
	} else {
		dst.Write(r.Action.Code)
		fmt.Fprintf(dst, "\n")
	}
}

type Decl interface {
	process(*job, int)
}

func sortedKeysInt(m map[string]int) []string {
	var lst []string
	for k := range m {
		lst = append(lst, k)
	}
	sort.Strings(lst)
	return lst
}

// this is a great use case for generics; these two functions
// are having to be specialized (copy pasta) *WITHOUT ANY CHANGE
// TO THEIR BODY* because the code doesn't depend on the parameterized
// type

func sortedKeysTokeninfo(m map[string]*tokeninfo) []string {
	var lst []string
	for k := range m {
		lst = append(lst, k)
	}
	sort.Strings(lst)
	return lst
}
