package main

import (
	//"fmt"

	e "bitbucket.org/dkolbly/earley3"
)

// get recursive; we use a grammar to parse the grammar

func makeGrammar() *e.Lang {
	start := e.NonTerminal("start")
	decls := e.NonTerminal("decls")

	decl := e.NonTerminal("decl")
	rule := e.NonTerminal("rule")
	typedef := e.NonTerminal("typedef")
	packagedef := e.NonTerminal("packagedef")
	importdef := e.NonTerminal("importdef")
	startdef := e.NonTerminal("startdef")
	exportdef := e.NonTerminal("exportdef")

	opt_accept := e.NonTerminal("opt_accept")
	rhs := e.NonTerminal("rhs")
	action := e.NonTerminal("action")
	item := e.NonTerminal("item")
	prim_item := e.NonTerminal("prim_item")
	symbols := e.NonTerminal("symbols")
	strings := e.NonTerminal("strings")

	g := e.New(start)

	unary := func(m []e.Meaning) e.Meaning {
		return m[0]
	}

	g.Rule(start, decls).Then(unary)

	// this gets very meta/circular!
	g.Rule(decls).
		Then(func([]e.Meaning) e.Meaning {
			return []Decl{}
		})

	g.Rule(decls, decls, decl).
		Then(func(m []e.Meaning) e.Meaning {
			pre := m[0].([]Decl)
			post := m[1].(Decl)
			return append(pre, post)
		})

	g.Rule(decl, rule).Then(unary)
	g.Rule(decl, typedef).Then(unary)
	g.Rule(decl, packagedef).Then(unary)
	g.Rule(decl, importdef).Then(unary)
	g.Rule(decl, startdef).Then(unary)
	g.Rule(decl, exportdef).Then(unary)

	g.Rule(packagedef, PackageKeyword, Symbol).
		Then(func(m []e.Meaning) e.Meaning {
			lit := m[1].(Token).Datum.(string)
			return &PackageDef{
				Name: lit,
			}
		})

	g.Rule(importdef, ImportKeyword, String).
		Then(func(m []e.Meaning) e.Meaning {
			lit := m[1].(Token).Datum.(string)
			return &ImportDef{
				Package: lit,
			}
		})

	g.Rule(importdef, ImportKeyword, Symbol, String).
		Then(func(m []e.Meaning) e.Meaning {
			as := m[1].(Token).Datum.(string)
			lit := m[2].(Token).Datum.(string)
			return &ImportDef{
				Package: lit,
				As:      as,
			}
		})

	g.Rule(importdef, ImportKeyword, Dot, String).
		Then(func(m []e.Meaning) e.Meaning {
			lit := m[2].(Token).Datum.(string)
			return &ImportDef{
				Package: lit,
				As:      ".",
			}
		})

	// used for literal tokens; they don't get types
	//
	// %token ","
	g.Rule(typedef, TokenKeyword, strings, Semicolon).
		Then(func(m []e.Meaning) e.Meaning {
			return &TypeDef{
				LiteralToken: true,
				Symbols:      m[1].([]string),
				Line:         m[0].(Token).Line,
			}
		})

	// used for token categories; they get types, which
	// are the expected type of the lexeme's value
	//
	// the corpus of type matchers is provided as an input
	// to the grammar generator function
	//
	// %token {gotype} name ... ;
	g.Rule(typedef, TokenKeyword, Block, symbols, Semicolon).
		Then(func(m []e.Meaning) e.Meaning {
			return &TypeDef{
				TokenClass: true,
				Line:       m[0].(Token).Line,
				Symbols:    m[2].([]string),
				TypeSpec:   string(m[1].(Token).Datum.([]byte)),
			}
		})

	// used for literal tokens that we will want to extract
	// information from.  Declares the concrete type of the
	// appropriate token
	g.Rule(typedef, TokenKeyword, Block, strings, Semicolon).
		Then(func(m []e.Meaning) e.Meaning {
			return &TypeDef{
				LiteralToken: true,
				Line:         m[0].(Token).Line,
				Symbols:      m[2].([]string),
				TypeSpec:     string(m[1].(Token).Datum.([]byte)),
			}
		})

	// these are used for non terminals
	//
	// %type {gotype} name ... ;
	g.Rule(typedef, TypeKeyword, Block, symbols, Semicolon).
		Then(func(m []e.Meaning) e.Meaning {
			return &TypeDef{
				Line:     m[0].(Token).Line,
				Symbols:  m[2].([]string),
				TypeSpec: string(m[1].(Token).Datum.([]byte)),
			}
		})

	g.Rule(startdef, StartKeyword, symbols, Semicolon).
		Then(func(m []e.Meaning) e.Meaning {
			return &StartDef{
				Line:    m[0].(Token).Line,
				Symbols: m[1].([]string),
			}
		})

	g.Rule(exportdef, ExportKeyword, symbols, Semicolon).
		Then(func(m []e.Meaning) e.Meaning {
			return &ExportDef{
				Line:    m[0].(Token).Line,
				Symbols: m[1].([]string),
			}
		})

	g.Rule(rule, Symbol, Define, rhs, opt_accept, action).
		Then(func(m []e.Meaning) e.Meaning {
			return &Rule{
				File:   m[0].(Token).File,
				Line:   m[0].(Token).Line,
				Left:   m[0].(Token).Datum.(string),
				Right:  m[2].([]Item),
				Accept: m[3].(bool),
				Action: m[4].(*Reducer),
			}
		})

	g.Rule(opt_accept, AcceptKeyword).
		Then(func(m []e.Meaning) e.Meaning {
			return true
		})

	g.Rule(opt_accept).
		Then(func(m []e.Meaning) e.Meaning {
			return false
		})

	g.Rule(action, Block).
		Then(func(m []e.Meaning) e.Meaning {
			t := m[0].(Token)
			return &Reducer{
				Code: t.Datum.([]byte),
				Line: t.Line,
			}
		})

	g.Rule(rhs).
		Then(func(m []e.Meaning) e.Meaning {
			return []Item{}
		})

	g.Rule(rhs, rhs, item).
		Then(func(m []e.Meaning) e.Meaning {
			pre := m[0].([]Item)
			post := m[1].(Item)
			return append(pre, post)
		})

	g.Rule(symbols).
		Then(func(m []e.Meaning) e.Meaning {
			return []string{}
		})

	g.Rule(symbols, symbols, Symbol).
		Then(func(m []e.Meaning) e.Meaning {
			pre := m[0].([]string)
			post := m[1].(Token).Datum.(string)
			return append(pre, post)
		})

	g.Rule(strings).
		Then(func(m []e.Meaning) e.Meaning {
			return []string{}
		})

	g.Rule(strings, strings, String).
		Then(func(m []e.Meaning) e.Meaning {
			pre := m[0].([]string)
			post := m[1].(Token).Datum.(string)
			return append(pre, post)
		})

	// named primitives...  this handles things like x:STRING and
	// comma:"," and such which are needed to name pattern
	// elements that don't have natural names (either because
	// there are multiple occurrences, or because they are not
	// identifiers)
	g.Rule(item, Symbol, Colon, prim_item).
		Then(func(m []e.Meaning) e.Meaning {
			name := m[0].(Token).Datum.(string)
			arg := m[2].(Item)
			arg.Name = name
			return arg
		})
	// named primitives for which we also want the context

	g.Rule(item, prim_item).
		Then(func(m []e.Meaning) e.Meaning {
			return m[0]
		})

	g.Rule(prim_item, Symbol).
		Then(func(m []e.Meaning) e.Meaning {
			s := m[0].(Token).Datum.(string)
			return Item{
				Symbolic: s,
			}
		})

	g.Rule(prim_item, String).
		Then(func(m []e.Meaning) e.Meaning {
			s := m[0].(Token).Datum.(string)
			return Item{
				Terminal: s,
			}
		})

	return g
}

type Item struct {
	Symbolic string
	Terminal string
	Name     string
}

type Reducer struct {
	Line int
	Code []byte
}

type PackageDef struct {
	Name string
}

type ImportDef struct {
	Package string
	As      string
}

type TypeDef struct {
	TokenClass   bool
	LiteralToken bool
	Line         int
	Symbols      []string
	TypeSpec     string
}

type StartDef struct {
	Line    int
	Symbols []string
}

type ExportDef struct {
	Line    int
	Symbols []string
}

type Rule struct {
	File   string
	Line   int
	Left   string
	Right  []Item
	Action *Reducer
	Accept bool
}
