package main

import (
	"context"
	"fmt"
	"io"
	"regexp"
	"strconv"

	"bitbucket.org/dkolbly/earley3"
)

type Token byte

const (
	OPEN  = Token('(')
	CLOSE = Token(')')
	PLUS  = Token('+')
	MINUS = Token('-')
	FLIP  = Token('f')
)

func (t Token) Value() any {
	// for this simple example, we don't ascribe any meaning to
	// tokens.  In a real app, I usually arrange for the meaning
	// of fixed tokens to carry the source code location
	return nil
}

type exactMatch Token

// Match implements earley.TokenMatcher; it's job is to take a lexeme
// and decide whether it matches this particular token.  The way we've
// set this one up, this matcher only matches the specific tokens.
// earley.Lexeme is an interface for lexemes that includes a method
// Value to return the meaning associated with the token (which is
// available in the grammar, although the generated code actually does
// the conversion to the declared type for a given token.
func (x exactMatch) Match(scope earley.Scope, lexeme earley.Lexeme) bool {
	token, ok := lexeme.(Token)
	if !ok {
		return false
	}
	return Token(x) == token
}

type intLexeme int

func (i intLexeme) Value() any {
	return int(i)
}

type intMatcher struct {
}

func (intMatcher) String() string {
	return "INTEGER"
}

func (intMatcher) Match(scope earley.Scope, lexeme earley.Lexeme) bool {
	_, ok := lexeme.(intLexeme)
	return ok
}

type strLexeme int

func (s strLexeme) Value() any {
	return string(s)
}

func (strMatcher) String() string {
	return "STRING"
}

type strMatcher struct {
}

func (strMatcher) Match(scope earley.Scope, lexeme earley.Lexeme) bool {
	_, ok := lexeme.(strLexeme)
	return ok
}

// String implements earley.TokenMatcher; it's job is to provide a string
// representation of this matcher, which is used when reporting errors
// or debugging, so we can say things like:
//
//	expr -> . IDENT
//
// the "IDENT" part comes from the String() method of the token matcher
// that is the scannable
func (x exactMatch) String() string {
	if Token(x) == FLIP {
		return "\"flip\""
	} else {
		return fmt.Sprintf("%q", byte(x))
	}
}

func Parse(ctx context.Context, src string) (Expr, error) {
	// the generator produces a function, Lang, that takes a token
	// map and produces an earley3 "language" which is basically
	// just a ruleset

	// this can be done once for a given language
	l := Lang(map[string]earley.TokenMatcher{
		"(":       exactMatch(OPEN),
		")":       exactMatch(CLOSE),
		"+":       exactMatch(PLUS),
		"-":       exactMatch(MINUS),
		"flip":    exactMatch(FLIP),
		"INTEGER": intMatcher{},
		"STRING":  strMatcher{},
	})

	// scanner is something that knows how to Scan lexemes
	// out of some input
	scanner := &simpleScanner{
		data: src,
	}

	// the language can then be executed against a source of
	// tokens.
	//
	// We pass a normal Go context for cases where it is useful.
	// It is kept in the Earley context, but not really exposed to
	// the generated code.  The context is also handed to the
	// scanner, useful in case we are reading off something
	// cancelable like a socket
	meaning, err := l.Parse(ctx, scanner)
	if err != nil {
		return nil, err
	}
	return meaning.(Expr), nil
}

type simpleScanner struct {
	data string
}

type scanner struct {
	match   string
	convert func(string) (earley.Lexeme, error)
}

var scanners = []scanner{
	{
		match: `^\+`,
		convert: func(_ string) (earley.Lexeme, error) {
			return PLUS, nil
		},
	},
	{
		match: `^-`,
		convert: func(_ string) (earley.Lexeme, error) {
			return MINUS, nil
		},
	},
	{
		match: `^\(`,
		convert: func(_ string) (earley.Lexeme, error) {
			return OPEN, nil
		},
	},
	{
		match: `^\)`,
		convert: func(_ string) (earley.Lexeme, error) {
			return CLOSE, nil
		},
	},
	{
		match: `^flip`,
		convert: func(_ string) (earley.Lexeme, error) {
			return FLIP, nil
		},
	},
	{
		match: `^[0-9]+`,
		convert: func(data string) (earley.Lexeme, error) {
			n, err := strconv.ParseInt(data, 10, 64)
			if err != nil {
				return nil, err
			}
			return intLexeme(n), nil
		},
	},
}

func (s *simpleScanner) Scan(_ context.Context) (earley.Lexeme, error) {
	if len(s.data) == 0 {
		return nil, io.EOF
	}

	for _, scanner := range scanners {
		// yikes!  but just for demo purposes..
		re := regexp.MustCompile(scanner.match)
		if match := re.FindString(s.data); match != "" {
			s.data = s.data[len(match):]
			return scanner.convert(match)
		}
	}
	return nil, fmt.Errorf("invalid token starting at %q", s.data)
}
