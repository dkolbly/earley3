%package main // -*-go-*-

// import some dependencies
%import "strings"

// define some tokens
%token {intLexeme} INTEGER;
%token {strLexeme} STRING;
%token {Token} "+" "-" "flip" "(" ")";

// define the types for some rules
%type {Expr} start expr;

// define some rules.  Have to start
// with "start"

start ::= expr {}

expr ::= n:INTEGER {
	return literalInt{int(n)}
}

expr ::= s:STRING {
	return literalStr{string(s)}
}

expr ::= l:expr "+" r:expr {
	return combo("+", l, r)
}

expr ::= l:expr "-" r:expr {
	return combo("-", l, r)
}

expr ::= "flip" "(" e:expr ")" {
	// a silly example just to use the strings package we bothered
	// to import
	return combo(strings.ToUpper("flip"), e)
}

