package main

import (
	"io"
	"fmt"
)

type Expr interface {
	WriteTo(dst io.Writer)
}

type literalInt struct {
	value int
}

func (l literalInt) WriteTo(dst io.Writer) {
	fmt.Fprintf(dst, "%d", l.value)
}

type literalStr struct {
	value string
}

func (l literalStr) WriteTo(dst io.Writer) {
	fmt.Fprintf(dst, "%q", l.value)
}

type combination struct {
	head string
	args []Expr
}

func combo(head string, args ...Expr) Expr {
	return &combination{
		head: head,
		args: args,
	}
}

func (c *combination) WriteTo(dst io.Writer) {
	fmt.Fprintf(dst, "(%s", c.head)
	for _, arg := range c.args {
		fmt.Fprintf(dst, " ")
		arg.WriteTo(dst)
	}
	fmt.Fprintf(dst, ")")
	return
}
