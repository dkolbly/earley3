package main

import (
	"context"
	"fmt"
	"os"
)

func main() {
	ctx := context.Background()

	for _, arg := range os.Args[1:] {
		expr, err := Parse(ctx, arg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "parse failed: %s\n", err)
			os.Exit(1)
		}
		expr.WriteTo(os.Stdout)
		os.Stdout.Write([]byte{'\n'})
	}
}
