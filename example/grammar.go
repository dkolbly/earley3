package main

import (
	"strings"
	"fmt"
	e "bitbucket.org/dkolbly/earley3"
)
func Lang(tokens map[string]e.TokenMatcher) *e.Lang {
	nt1 := e.NonTerminal("expr")
	nt0 := e.NonTerminal("start")
	musttoken := func(n string) e.TokenMatcher {
		tm := tokens[n]
		if tm == nil {
			panic(fmt.Sprintf("token matcher for %q not provided", n))
		}
		return tm
	}
	tl5 := musttoken("(")
	tl6 := musttoken(")")
	tl2 := musttoken("+")
	tl3 := musttoken("-")
	tl4 := musttoken("flip")
	tc0 := musttoken("INTEGER")
	tc1 := musttoken("STRING")
	// init grammar with start symbols
	g := e.New(
		nt0, // start
		)
	// grammar.el, line 17
	//
	//  start ::=
	//        expr
	//
	g.Rule(nt0 /* ::= */, nt1).
		Then(func(_m []e.Meaning) e.Meaning {
			expr := _m[0].(Expr)
			return _Langreduce_0(expr)
		})
	// grammar.el, line 19
	//
	//  expr ::=
	//        INTEGER
	//
	g.Rule(nt1 /* ::= */, tc0).
		Then(func(_m []e.Meaning) e.Meaning {
			n := _m[0].(intLexeme)
			return _Langreduce_1(n)
		})
	// grammar.el, line 23
	//
	//  expr ::=
	//        STRING
	//
	g.Rule(nt1 /* ::= */, tc1).
		Then(func(_m []e.Meaning) e.Meaning {
			s := _m[0].(strLexeme)
			return _Langreduce_2(s)
		})
	// grammar.el, line 27
	//
	//  expr ::=
	//        expr
	//        "+"
	//        expr
	//
	g.Rule(nt1 /* ::= */, nt1, tl2, nt1).
		Then(func(_m []e.Meaning) e.Meaning {
			l := _m[0].(Expr)
			r := _m[2].(Expr)
			return _Langreduce_3(l, r)
		})
	// grammar.el, line 31
	//
	//  expr ::=
	//        expr
	//        "-"
	//        expr
	//
	g.Rule(nt1 /* ::= */, nt1, tl3, nt1).
		Then(func(_m []e.Meaning) e.Meaning {
			l := _m[0].(Expr)
			r := _m[2].(Expr)
			return _Langreduce_4(l, r)
		})
	// grammar.el, line 35
	//
	//  expr ::=
	//        "flip"
	//        "("
	//        expr
	//        ")"
	//
	g.Rule(nt1 /* ::= */, tl4, tl5, nt1, tl6).
		Then(func(_m []e.Meaning) e.Meaning {
			e := _m[2].(Expr)
			return _Langreduce_5(e)
		})
	return g
}

//--------------------------------------
// grammar.el, line 17
//
//  start ::=
//        expr
//
//line grammar.el:17
func _Langreduce_0(expr Expr) Expr {
	// synthesized body
	return expr
}

//--------------------------------------
// grammar.el, line 19
//
//  expr ::=
//        INTEGER
//
//line grammar.el:19
func _Langreduce_1(n intLexeme) Expr {
	return literalInt{int(n)}
}

//--------------------------------------
// grammar.el, line 23
//
//  expr ::=
//        STRING
//
//line grammar.el:23
func _Langreduce_2(s strLexeme) Expr {
	return literalStr{string(s)}
}

//--------------------------------------
// grammar.el, line 27
//
//  expr ::=
//        expr
//        "+"
//        expr
//
//line grammar.el:27
func _Langreduce_3(l Expr, r Expr) Expr {
	return combo("+", l, r)
}

//--------------------------------------
// grammar.el, line 31
//
//  expr ::=
//        expr
//        "-"
//        expr
//
//line grammar.el:31
func _Langreduce_4(l Expr, r Expr) Expr {
	return combo("-", l, r)
}

//--------------------------------------
// grammar.el, line 35
//
//  expr ::=
//        "flip"
//        "("
//        expr
//        ")"
//
//line grammar.el:35
func _Langreduce_5(e Expr) Expr {
	// a silly example just to use the strings package we bothered
	// to import
	return combo(strings.ToUpper("flip"), e)
}
