package earley

import (
	"context"
)

type Lexeme interface {
	//TokenType() TokenTypeCode
	Value() interface{}
}

/*type TokenTypeCode interface {
	String() string
}*/

type Scanner interface {
	Scan(context.Context) (Lexeme, error)
}
